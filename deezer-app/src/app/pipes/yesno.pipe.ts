import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'yesno'
})
export class YesNoPipe implements PipeTransform {
    transform(value: boolean, camelCase: boolean) {
        var result = value ? "Yes" : "No";
        return camelCase 
            ? result 
            : result.toLowerCase();
    }
}