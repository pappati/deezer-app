import { trigger, state, animate, transition, style } from "@angular/animations";

export const FadeInAnimation = trigger(
    "FadeInAnimation",
    [
        //route "enter" transition
        transition(":enter", [
            //styles at start of transition
            style({ opacity: 0 }),
            //animation and styles at the end og transition
            animate(".3s", style({ opacity: 1 }))
        ])
    ]
);