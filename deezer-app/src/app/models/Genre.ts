export interface Genre {
    id: number;
    name: string;
    picture: string;
    type: string;
}

export interface Genres {
    data: Genre[];
}