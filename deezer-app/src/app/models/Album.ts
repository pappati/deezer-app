import { Genres, Contributor, Artist, Tracks } from "./index";

export interface Album {
    id: number;
    title: string;
        upc: string;
    cover: string;
    cover_small: string;
    cover_medium: string;
    cover_big: string;
    cover_xl: string;
        genre_id: number;
        genres: Genres;
        label: string;
        nb_tracks: number;
        duration: number;
        fans: number;
        rating: number;
        release_date: string;
        record_type: string;
        available: boolean;
    tracklist: string;
        explicit_lyrics: boolean;
        contributors: Contributor[];
        artist: Artist;
    type: string;
        tracks: Tracks;
}