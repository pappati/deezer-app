import { Album, Artist } from "app/models/Index";

export interface Track {
    id: number;
    readable: boolean;
    title: string;
    title_short: string;
    title_version: string;
    link: string;
    duration: number;
    rank: number;
    explicit_lyrics: boolean;
    preview: string;
    artist: Artist;
    album: Album;
    type: string;
}

export interface Tracks {
    data: Track[];
}