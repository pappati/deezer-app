import { Component, Input } from "@angular/core";
import { Album, Track } from "app/models/index";

@Component({
  selector: 'track-list',
  templateUrl: './track-list.component.html',
  styleUrls: ['./track-list.component.scss']
})
export class TrackListComponent {
    @Input() tracks: Track[];
}