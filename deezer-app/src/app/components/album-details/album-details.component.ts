import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DomSanitizer, SafeStyle } from "@angular/platform-browser";

import { SlideInOutAnimation } from "app/animations";
import { Album } from "app/models/index";
import { DeezerService } from "app/services/index";

@Component({
  selector: 'album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss'],
  // animations: [SlideInOutAnimation],
  // host: {"[@SlideInOutAnimation]":""},
  encapsulation: ViewEncapsulation.None
})
export class AlbumDetailsComponent implements OnInit {
  private album: Album;
  private selectedTabIndex: number;

  constructor(
    private _route:ActivatedRoute,
    private _router:Router, 
    private _domSanitizer: DomSanitizer,
    private _deezerService: DeezerService) { }

  ngOnInit() {
    let albumId = Number(this._route.snapshot.params["id"]);
    if(albumId)
      this.getAlbum(albumId);
  }

  getAlbum(id: number) {
    this._deezerService.getAlbum(id)
        .subscribe(
          data => { this.album = data },
          error => { console.error(error) });
  }

  getAlbumBackground(): SafeStyle {
    if (this.album)
      return this._domSanitizer.bypassSecurityTrustStyle("url("+this.album.artist.picture_small+")");
    else return this._domSanitizer.bypassSecurityTrustStyle("url()");    
  }

  selectTab(tabIndex: number) {
    // we have 3 tabs currently (0 is back button)
    if (tabIndex >= 3) tabIndex = 1;
    if (tabIndex < 0)  tabIndex = 1;
    this.selectedTabIndex = tabIndex;
  }

  //if the user changes tab with the tabs, not by our code
  onTabChanged(event: any) {
    this.selectedTabIndex = event.index;
  }

  isArtistInContributors(): boolean {
    if (!this.album)
      return true;
    
    var found: boolean = false;
    this.album.contributors.forEach(c => {
      if (c.name.toLowerCase() == this.album.artist.name.toLowerCase())
        found = true;
    });
    return found;
  }
}