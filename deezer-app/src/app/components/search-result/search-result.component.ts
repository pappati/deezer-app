import { Component, OnInit, Input } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

import { Track } from "app/models/Index";

@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
  @Input() track: Track;
  constructor(private _domSanitizer: DomSanitizer) { }

  ngOnInit() { }

  getAtristBackground() {
    return this._domSanitizer.bypassSecurityTrustStyle("url(" + this.track.artist.picture_small + ")");
  }
}