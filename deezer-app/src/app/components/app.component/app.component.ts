import { Component } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private _router: Router){}
  getCurrentRoutedUrl() {
    console.log("this._router.url", this._router.url);
    return this._router.url;
  }

  isOnHome():boolean {
    var current = this.getCurrentRoutedUrl();
    return current == "/" || current == "";
  }
}