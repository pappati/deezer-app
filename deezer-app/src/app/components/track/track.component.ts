import { Component, Input } from "@angular/core";
import { Album, Track } from "app/models/index";

@Component({
  selector: 'track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent {
    @Input() t: Track;
}