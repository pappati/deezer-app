import { Component, AfterViewInit, ChangeDetectionStrategy, EventEmitter, NgZone, ChangeDetectorRef, ViewChild, ElementRef } from "@angular/core";
import { FormControl, ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/observable/fromEvent";
import { NgFor } from "@angular/common";

import { DeezerService } from "app/services/index";
import { Track } from "app/models/Index";

@Component({
  selector: 'search-track',
  templateUrl: './search-track.component.html',
  styleUrls: ['./search-track.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchTrackComponent implements AfterViewInit {
  @ViewChild("searchInput") searchInput: ElementRef;
  tracksObs: Observable<Track[]>;
  tracks: Track[];
  inputSearchTerm: string = null;
  tracksChanged  = new EventEmitter();

  constructor(private deezerservice: DeezerService, 
              private ngzone: NgZone, 
              private cdref: ChangeDetectorRef, 
              private _route:ActivatedRoute) { }

  ngAfterViewInit() {
    let searchTerm = this._route.snapshot.params["term"];
    console.log("search term from router:", searchTerm);
    if (searchTerm)
      this.inputSearchTerm = searchTerm;
    this.getTracks();

    this.ngzone.runOutsideAngular( () => {
      Observable.fromEvent(this.searchInput.nativeElement, 'keyup')
        .debounceTime(500)
        .subscribe(keyboardEvent => {
          this.getTracks();
          this.cdref.detectChanges();
        });
    });
  }

  getTracks() {
    this.tracks = null;
    this.tracksObs = this.deezerservice.getTracks(this.inputSearchTerm);
    this.tracksObs.subscribe(
      data => { this.clearAndAddNewTracks(data) },
      error => { console.error(error) });
  }

  clearAndAddNewTracks(newTracks: Track[]) {
    // this.tracks = new Array<Track>();
    // newTracks.forEach(t => this.tracks.push(t));
    this.tracks = newTracks;
    this.cdref.detectChanges();
  }

  trackTracker(index, track: Track) {
    return track ? track.id : undefined;
  }
}