export class AppSettings {
    static ApiEndpoints = {
        TrackUrl: "https://deezerdevs-deezer.p.mashape.com/search?q={term}",
        AlbumUrl: "https://deezerdevs-deezer.p.mashape.com/album/{id}", //The Deezer album id
        ArtistUrl: "https://deezerdevs-deezer.p.mashape.com/artist/{id}" //The artist's Deezer id
    };
    static ApiAuthentication = {
        HeaderName: "X-Mashape-Key", 
        ApiKey: "v1bgDnEne1mshUXeAG1GcCR5t0nvp1zZwhBjsnMsnroAQ1D0ar"
    };
    static DefaultSearchTerm: string = "Metallica";
}