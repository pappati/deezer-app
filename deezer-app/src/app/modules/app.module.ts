import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { Routes, RouterModule, UrlSerializer } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

//material
//@see https://material.angular.io/guide/getting-started
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule, MdCardModule, MdButtonModule, MdCheckboxModule, MdInputModule, MdProgressBar, 
         MdTabsModule, MdTooltipModule } from "@angular/material";
import "hammerjs"; //adds gesture support

//components
import { AlbumDetailsComponent, AppComponent, SearchTrackComponent, SearchResultComponent,
         TrackListComponent, TrackComponent } from "../components/index";
//services
import { DeezerService } from "../services/index";
//pipes
import { SortPipe, SecondsToTimePipe, YesNoPipe } from "../pipes/index";
//helpers
import { LowerCaseUrlSerializer } from "../helpers/index";

//routing
const routes: Routes = [
  { path: "", pathMatch: "full", component: SearchTrackComponent },
  { path: "search/:term", component: SearchTrackComponent },
  { path: "search", component: SearchTrackComponent },
  { path: "album/:id", component: AlbumDetailsComponent },
  { path: "**", redirectTo: "" }
];

@NgModule({
  declarations: [
    //components
    AppComponent,
    SearchTrackComponent,
    SearchResultComponent,
    AlbumDetailsComponent,
    TrackListComponent,
    TrackComponent,
    //pipes
    SortPipe,
    SecondsToTimePipe, 
    YesNoPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    //material
    MaterialModule,
    BrowserAnimationsModule, //this MUST always come before other material modules
    MdCardModule, MdButtonModule, MdCheckboxModule, MdInputModule, MdTabsModule, MdTooltipModule
  ],
  exports: [
    RouterModule,
    //material
    MaterialModule,
    MdCardModule, MdButtonModule, MdCheckboxModule, MdInputModule, MdTabsModule, MdTooltipModule
  ],
  providers: [
    //services
    DeezerService,
    //override default url serialization to custom
    {
      provide: UrlSerializer,
      useClass: LowerCaseUrlSerializer
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class RoutedComponents { AppComponent };