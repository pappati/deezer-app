import { Directive, Input, TemplateRef, ViewContainerRef, Renderer } from "@angular/core";

@Directive({ selector: "[hideIf]" })
export class HideIfDirective {
    private visible = false;

    constructor(
        private _templateRef: TemplateRef<any>,
        private _viewContainer: ViewContainerRef,
        private _renderer: Renderer) { }

    @Input() set hideIf(condition: boolean) {
        this._viewContainer.createEmbeddedView(this._templateRef);
        if (!condition && !this.visible)
            this.setVisibility(true);
        else if (condition && this.visible)
            this.setVisibility(false);
    }

    setVisibility(vis: boolean) {
        this.visible = vis;
        this._renderer.setElementStyle(this._templateRef.elementRef.nativeElement, "display", vis ? "inherited" : "none");
    }
}