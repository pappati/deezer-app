import { TestBed, inject } from '@angular/core/testing';

import { DeezerService } from './index';

describe('DeezerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeezerService]
    });
  });

  it('should ...', inject([DeezerService], (service: DeezerService) => {
    expect(service).toBeTruthy();
  }));
});
