import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

import { Album, Artist, Track } from "app/models/Index";
import { AppSettings } from "app/app.settings";

@Injectable()
export class DeezerService {
  constructor(private http: Http) { }

  getTracks(queryString): Observable<Track[]> {
    let url = AppSettings.ApiEndpoints.TrackUrl.replace("{term}", queryString || AppSettings.DefaultSearchTerm);
    return this.callApi(url, this.extractTracks);
  }
    private extractTracks(res: Response): Promise<Track[]> {
    let body = res.json();
    let result = body.data || {};
    console.log("Track", result);
    return result;
  }

  getAlbum(id: number): Observable<Album> {
    let url = AppSettings.ApiEndpoints.AlbumUrl.replace("{id}", id.toString()/* || "14590596"*/);
    return this.callApi(url, this.extractAlbum);
  }
  private extractAlbum(res: Response): Promise<Album> {
    let body = res.json();
    let result = body || {};
    console.log("Album", result);
    return result;
  }

  getArtist(id: number): Observable<Artist> {
    let url = AppSettings.ApiEndpoints.ArtistUrl.replace("{id}", id.toString() || "119");
    return this.callApi(url, this.extractArtist);
  }
  private extractArtist(res: Response): Promise<Artist> {
    let body = res.json();
    let result = body || {};
    console.log("Artist", result);
    return result;
  }

  private getAuthData() {
    let headers = new Headers();
    headers.append(AppSettings.ApiAuthentication.HeaderName, AppSettings.ApiAuthentication.ApiKey);
    let options = new RequestOptions({ headers: headers });
    return options;
  }

  private callApi(url, mapDelegate): Observable<any> {
    return this.http.get(url, this.getAuthData())
      .share()
      .map(mapDelegate)
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}