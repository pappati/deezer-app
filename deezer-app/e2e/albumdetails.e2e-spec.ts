import { DeezerAppPage } from './app.po';

describe('deezer-app App', function() {
  let page: DeezerAppPage;

  beforeEach(() => {
    page = new DeezerAppPage();
  });

  it('should display album name', () => {
    page.navigateToAlbumDetailsById(1649120);
    page.getAlbumTitle().then(console.log)
    expect(page.getAlbumTitle()).toEqual('The Eminem Show');
  });
});
