import { browser, element, by } from 'protractor';

export class DeezerAppPage {
  navigateTo() {
    return browser.get('/');
  }

    navigateToAlbumDetailsById(id: number) {
      console.log(id);
      var url = '/album/' + id;
      console.log(url)
    return browser.get(url);
  }

  getAlbumTitle()  {
    return element(by.id('album_title')).getText();
  }
 }
